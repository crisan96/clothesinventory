package com.example.helloworld;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BluzaActivity extends AppCompatActivity {

    private ImageSwitcher imagesIs;
    private Button previousBtn;
    private Button nextBtn;
    private Button pickImagesBtn;
    private Button deleteBtn;

    //public ArrayList<Uri> imageUris;
    public ArrayList<Bitmap> imageBts;

    private static final int PICK_IMAGE_CODE = 0;

    int position = 0;

    private static final String STATE_KEY = "BLUZA_INVENTORY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeans);

        imagesIs = findViewById(R.id.imagesIs);
        previousBtn = findViewById(R.id.previousBtn);
        nextBtn = findViewById(R.id.nextBtn);
        deleteBtn = findViewById(R.id.deleteButton);
        pickImagesBtn = findViewById(R.id.pickImageBtn);
        imagesIs.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView = new ImageView(getApplicationContext());
                return imageView;
            }
        });
/*        if(savedInstanceState!=null){
            imageUris = savedInstanceState.getParcelableArrayList(STATE_KEY);
        }else {
            imageUris = new ArrayList<>();
        }*/
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> dadada =  preferences.getStringSet(STATE_KEY, null);
        if(dadada != null && dadada.size() != 0) {
            Iterator<String> iterator = dadada.iterator();


            //imageUris = new ArrayList();
            imageBts = new ArrayList<>();

            while (iterator.hasNext()){
                String b64s = (iterator.next());
                byte[] b = Base64.decode(b64s, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                imageBts.add(bitmap);
            }

            Drawable myDrawable = new BitmapDrawable(getResources(), imageBts.get(0));
//            try {
//                InputStream inputStream = getContentResolver().openInputStream(imageUris.get(0));
//                myDrawable = Drawable.createFromStream(inputStream, imageUris.get(0).toString());
//            }catch (Exception e){
//                System.out.println("CACATTTTT");
//            }
            imagesIs.setImageDrawable(myDrawable);

        }
        else {
            imageBts = new ArrayList<>();

        }


        pickImagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImagesIntent();
            }
        });

        previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position > 0) {
                    --position;
                    Drawable myDrawable =  new BitmapDrawable(getResources(), imageBts.get(position));

                    //imagesIs.setImageURI((imageUris.get(position)));
                    imagesIs.setImageDrawable(myDrawable);

                }else {
                    Toast.makeText(BluzaActivity.this, "NO previous photo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position < imageBts.size() - 1) {
                    ++position;
                    Drawable myDrawable =new BitmapDrawable(getResources(), imageBts.get(position));

                    //imagesIs.setImageURI((imageUris.get(position)));
                    imagesIs.setImageDrawable(myDrawable);
                }else {
                    Toast.makeText(BluzaActivity.this, "NO more photo", Toast.LENGTH_SHORT).show();
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageBts.size() == 0){
                    Toast.makeText(BluzaActivity.this, "NO PHOTO TO DELETE", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(imageBts.size() == 1){
                    imageBts.remove(0);

                    imagesIs.setImageDrawable(getResources().getDrawable(R.drawable.noimg));

                    position = 0;

                    return;
                }

                if(position == 0){

                    imageBts.remove(0);
                    Drawable myDrawable = new BitmapDrawable(getResources(), imageBts.get(0));
                    imagesIs.setImageDrawable(myDrawable);

                    return;
                }

                if(position == imageBts.size() - 1){
                    imageBts.remove(imageBts.size() - 1);
                    Drawable myDrawable = new BitmapDrawable(getResources(), imageBts.get(imageBts.size() - 1));
                    imagesIs.setImageDrawable(myDrawable);

                    position = imageBts.size() - 1;

                    return;
                }

                imageBts.remove(position);
                Drawable myDrawable = new BitmapDrawable(getResources(), imageBts.get(position));
                imagesIs.setImageDrawable(myDrawable);
            }
        });

    }

    private void pickImagesIntent(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image(s)"), PICK_IMAGE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_CODE){
            if(resultCode == Activity.RESULT_OK){
                if(data.getClipData()!=null){
                    int count = data.getClipData().getItemCount();
                    for(int i = 0; i < count; ++i){
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                            bitmap = getResizedBitmap(bitmap, 4000);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        imageBts.add(bitmap);
                    }
                }
                else {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                        bitmap = getResizedBitmap(bitmap, 4000);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageBts.add(bitmap);
                }

                Drawable myDrawable = new BitmapDrawable(getResources(), imageBts.get(0));
                //imagesIs.setImageURI((imageUris.get(position)));
                imagesIs.setImageDrawable(myDrawable);
                //imagesIs.setImageURI(imageUris.get(0));
                position = 0;
            }
        }
    }

/*    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putParcelableArrayList(STATE_KEY, imageUris);
    }

    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);

        if(savedInstanceState != null){
            imageUris = savedInstanceState.getParcelableArrayList(STATE_KEY);
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();

        // Store values between instances here
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        Set<String> goodSet = new HashSet<String>();
        for (Bitmap btm : imageBts){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            btm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            goodSet.add(encodedImage);
        }

        //Set<String> set = new HashSet(imageUris);
        editor.putStringSet(STATE_KEY, goodSet);

        editor.commit();

    }

    /**
     * reduces the size of the image
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}