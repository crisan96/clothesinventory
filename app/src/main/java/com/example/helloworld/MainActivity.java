package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton jeansCategory;
    ImageButton pantaloniTriSfertCategory;
    ImageButton bluzaCatogory;
    ImageButton tricouCategory;
    ImageButton puloverCategory;
    ImageButton camasutzaCategory;
    ImageButton sportCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jeansCategory = (ImageButton) findViewById(R.id.btnCategoryBlugi);
        pantaloniTriSfertCategory = (ImageButton) findViewById(R.id.butonTriSfert);
        bluzaCatogory = (ImageButton) findViewById(R.id.btnCategoryBluze);
        tricouCategory = (ImageButton) findViewById(R.id.btnCategoryTricouri);
        puloverCategory = (ImageButton) findViewById(R.id.btnCategoryPulovere);
        camasutzaCategory = (ImageButton) findViewById(R.id.btnCategoryCamasi);
        sportCategory = (ImageButton) findViewById(R.id.btnCategorySports);


        jeansCategory.setOnClickListener(v -> {
            Intent jeansActivity = new Intent(MainActivity.this, JeansActivity.class);
            startActivity(jeansActivity);
        });

        pantaloniTriSfertCategory.setOnClickListener(v -> {
            Intent pantaloniTriSfert = new Intent(MainActivity.this, PantaloniTriSfertActivity.class);
            startActivity(pantaloniTriSfert);
        });

        bluzaCatogory.setOnClickListener(v -> {
            Intent bluzaCatogory = new Intent(MainActivity.this, BluzaActivity.class);
            startActivity(bluzaCatogory);
        });

        tricouCategory.setOnClickListener(v -> {
            Intent tricouCategory = new Intent(MainActivity.this, TricouActivity.class);
            startActivity(tricouCategory);
        });

        puloverCategory.setOnClickListener(v -> {
            Intent puloverCategory = new Intent(MainActivity.this, PuloverActivity.class);
            startActivity(puloverCategory);
        });

        camasutzaCategory.setOnClickListener(v -> {
            Intent camasutzaCategory = new Intent(MainActivity.this, CamasutzaActivity.class);
            startActivity(camasutzaCategory);
        });

        sportCategory.setOnClickListener(v -> {
            Intent sportCategory = new Intent(MainActivity.this, SportActivity.class);
            startActivity(sportCategory);
        });

    }
}